package pl.edu.pwsztar.service.impl;

import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.service.ChessService;

import java.util.Arrays;

@Service
public class ChessServiceImpl implements ChessService {
    @Override
    public boolean isMoveCorrect(FigureMoveDto figureMoveDto) {
        switch (figureMoveDto.getType()){
            case BISHOP:
                return canBishopMove(figureMoveDto);
            default:
                return false;
        }

    }

    private boolean canBishopMove(FigureMoveDto figureMoveDto){
        final int startPos[] = Arrays.stream(figureMoveDto.getStart().split("_"))
                .mapToInt(word -> word.charAt(0))
                .toArray();

        final int destinationPos[] = Arrays.stream(figureMoveDto.getDestination().split("_"))
                .mapToInt(word -> word.charAt(0))
                .toArray();

        final int firstPos = changeNegativeNumber(startPos[0]-destinationPos[0]);
        final int secPos = changeNegativeNumber(startPos[1]-destinationPos[1]);

        if(firstPos==secPos){
            return true;
        }else{
            return false;
        }
    }

    private int changeNegativeNumber(int number){
        return number < 0 ? number*(-1) : number;
    }
}
